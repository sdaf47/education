<?php

use WS\Orm\DataBase;

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = include __DIR__.'/vendor/autoload.php';
$loader->addPsr4("WS\\", __DIR__."/lib/");
$db = DataBase::getInstance();
$db->setConnection(require_once "config.php");