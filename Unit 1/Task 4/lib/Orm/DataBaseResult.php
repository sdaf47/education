<?php

namespace WS\Orm;

use \mysqli_result;

class DataBaseResult {

    /** @var mysqli_result $result */
    private $result;

    /** @var  ActiveRecord */
    private $className;

    /** @var array $cache */
    private static $cache = [];

    /**
     * DataBaseResult constructor.
     * @param string $className
     * @param string $query
     */
    public function __construct($className, $query) {
        $index = md5($query);
        if (!key_exists($index, self::$cache)) {
            self::$cache[$index] = DataBase::getInstance()->query($query);
        }
        self::$cache[$index] = DataBase::getInstance()->query($query);
        $this->result = self::$cache[$index];
        $this->className = $className;
    }

    /**
     * @return bool|ActiveRecord
     */
    public function fetch() {
        $result = $this->result->fetch_assoc();
        if (!$result) {
            return false;
        }
        return new $this->className($result);
    }
}