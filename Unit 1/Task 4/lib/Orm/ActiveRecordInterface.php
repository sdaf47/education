<?php

namespace WS\Orm;

interface ActiveRecordInterface {
    /**
     * EntityInterface constructor.
     * @param array $data
     */
    public function __construct(array $data = []);

    /**
     * @return string
     */
    public static function getTableName();

    /**
     * @return array
     */
    public static function map();
}