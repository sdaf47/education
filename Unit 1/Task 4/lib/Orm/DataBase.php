<?php

namespace WS\Orm;

use Exception;
use mysqli;

class DataBase
{
    const DB_NAME = "DB_NAME";
    const DB_USER = "DB_USER";
    const DB_PASS = "DB_PASS";
    const DB_HOST = "DB_HOST";

    /** @var mysqli $connect */
    private $connect;

    /** @var DataBase $instance */
    private static $instance;

    private function __construct() {}

    public function setConnection(array $config) {
        $this->connect = new mysqli(
            $config[self::DB_HOST],
            $config[self::DB_USER],
            $config[self::DB_PASS],
            $config[self::DB_NAME]
        );
    }

    /**
     * @return string
     */
    public function getError() {
        return "DB ERROR: " . $this->connect->connect_error;
    }

    /**
     * @return DataBase
     * @internal param array $config
     */
    public static function getInstance() {
        if (!self::$instance) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function query($query) {
        if (!($this->connect instanceof mysqli)) {
            throw new Exception("Not found connection");
        }
        $result = $this->connect->query($query);
        if (!$result) {
            throw new Exception("Query result is false");
        }
        return $result;
    }
}