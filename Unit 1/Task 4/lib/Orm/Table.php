<?php

namespace WS\Orm;

class Table {

    /**
     * @param ActiveRecord $className
     * @return array
     */
    public static function map($className) {
        /** @var ActiveRecord $className */
        return $className::map();
    }

    /**
     * @param ActiveRecord $className
     * @return string
     */
    public static function getTableName($className) {
        /** @var ActiveRecord $className */
        return $className::getTableName();
    }

    /**
     * @param ActiveRecord $className
     * @param array $data
     * @return ActiveRecord|bool
     */
    public static function create($className, $data = []) {
        if (!$data) {
            return false;
        }
        return new $className($data);
    }
}