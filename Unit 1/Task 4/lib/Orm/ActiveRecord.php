<?php

namespace WS\Orm;
use \mysqli_result;

/**
 * Class ActiveRecord
 * @package WS\Orm
 */
abstract class ActiveRecord implements ActiveRecordInterface
{
    const FIELD_ID = "id";

    protected $data = [];

    /** @var mysqli_result $result */
    protected static $result;

    /**
     * ActiveRecord constructor.
     * @param array $data
     */
    public function __construct(array $data = []) {
        foreach (Table::map(get_called_class()) as $field) {
            if (key_exists($field, $data)) {
                $this->data[$field] = $data[$field];
            }
        };
    }

    /**
     * @return array
     */
    public function getData() {
        return $this->data;
    }

    /**
     * @param $query
     * @return DataBaseResult
     */
    private static function query($query) {
        self::$result = new DataBaseResult(
            get_called_class(),
            $query
        );

        return self::$result;
    }

    /**
     * @return array|bool
     */
    private function getStringMap() {
        foreach ($this->data as $field => $value) {
            if ($value) {
                $fields[] = $field;
                $values[] = "'" . $value . "'";
            }
        }
        if (empty($values) || empty($fields)) {
            return false;
        }
        $strFields = implode(",", $fields);
        $strValues = implode(",",$values);
        return [$strFields, $strValues];
    }

    /**
     * @param $field
     * @param $value
     * @return DataBaseResult
     */
    public static function findBy($field, $value) {
        return self::query(sprintf(
            "SELECT * FROM %s WHERE %s='%s'",
            Table::getTableName(get_called_class()),
            $field,
            $value
        ));
    }

    /**
     * @return DataBaseResult
     */
    public static function all() {
        return self::query(sprintf(
            "SELECT * FROM %s",
            Table::getTableName(get_called_class())
        ));
    }

    /**
     * @param $id
     * @return DataBaseResult
     */
    public static function findByPrimary($id) {
        return self::query(sprintf(
            "SELECT * FROM %s WHERE id=%s",
            Table::getTableName(get_called_class()),
            $id
        ));
    }

    /**
     * @return bool
     */
    public function save() {
        list($fields, $values) = $this->getStringMap();
        $result = self::query(sprintf(
            "INSERT INTO %s(%s) VALUES(%s)",
            Table::getTableName(get_called_class()),
            $fields,
            $values
        ));
        if ($result) {
            return true;
        }
        return false;
    }

    public function delete() {
        self::query(sprintf(
            "DELETE FROM %s WHERE id=%s",
            Table::getTableName(get_called_class()),
            $this->getPrimary()
        ));
    }

    /**
     * @return mixed
     */
    public function getPrimary() {
        return $this->data[self::FIELD_ID];
    }

    public function __get($field) {
        return $this->data[$field];
    }

    public function __set($field, $value) {
        $this->data[$field] = $value;
    }
}