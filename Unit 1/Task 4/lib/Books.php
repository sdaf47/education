<?php

namespace WS;

use WS\Orm\ActiveRecord;

class Books extends ActiveRecord
{
    const TABLE_NAME = "books";

    const FIELD_TITLE = "title";
    const FIELD_AUTHOR = "author";
    const FIELD_DESCRIPTION = "description";

    /**
     * @return string
     */
    public static function getTableName() {
        return self::TABLE_NAME;
    }

    /**
     * @return array
     */
    public static function map()
    {
        return [
            self::FIELD_ID,
            self::FIELD_TITLE,
            self::FIELD_AUTHOR,
            self::FIELD_DESCRIPTION
        ];
    }
}