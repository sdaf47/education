<?php

use WS\Books;

class ActiveRecordTest extends PHPUnit_Framework_TestCase {

    const ASSERT_AUTHOR = "Pisatel";

    public function testAddNewRecord() {
        $book = new Books([
            Books::FIELD_TITLE => "Kniga",
            Books::FIELD_AUTHOR => self::ASSERT_AUTHOR,
            Books::FIELD_DESCRIPTION => "Kniga"
        ]);
        $book->save();
        $this->assertEquals($book->author, self::ASSERT_AUTHOR);
    }

    public function testFindAll() {
        $bookResult = Books::all();
        $book = $bookResult->fetch();
        $this->assertInstanceOf(Books::class, $book);
    }

    public function testFindRecord() {
        $book = Books::findBy(Books::FIELD_AUTHOR, self::ASSERT_AUTHOR)->fetch();
        $this->assertEquals($book->author, self::ASSERT_AUTHOR);
    }

    public function testDeleteRecord() {
        $book = Books::findBy(Books::FIELD_AUTHOR, self::ASSERT_AUTHOR)->fetch();
        $book->delete();
        $res = Books::findByPrimary($book->getPrimary())->fetch();
        $this->assertEquals($res, false);
    }
}
