<?php
/** @var \Composer\Autoload\ClassLoader $loader */
use WS\Orm\DataBase;

$loader = include __DIR__.'/vendor/autoload.php';
$loader->addPsr4("WS\\", __DIR__."/lib/");
$db = DataBase::getInstance();
$db->setConnection(require_once "config.php");

$res = $db->query(
"CREATE TABLE books (
   id INT NOT NULL AUTO_INCREMENT,
   title VARCHAR(50) NULL,
   author VARCHAR(50) NULL,
   description VARCHAR(50) NULL,
   PRIMARY KEY (id)
);"
);

if ($error = $db->getError()) {
    echo "\n" . $error . "\n";
}