<?php

namespace WS\Education\Unit1\Task2;

/**
 * @author Maxim Sokolovsky <sokolovsky@worksolutions.ru>
 */
class Connection {

    private $resource;

    /**
     * Connection constructor.
     * @param resource $resource
     * @internal param $url
     * @internal param $port
     */
    public function __construct($resource)
    {
        $this->resource = $resource;
    }

    /**
     * @param int $length
     * @return string
     */
    public function read($length = 1024)
    {
        $string = "";
        socket_recv($this->resource, $string, $length, 0);
        return $string;
    }

    /**
     * @param string $string
     */
    public function write($string)
    {
        socket_write($this->resource, $string);
    }

    public function close()
    {
        socket_close($this->resource);
    }
}