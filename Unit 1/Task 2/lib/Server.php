<?php
/**
 * @author Maxim Sokolovsky <sokolovsky@worksolutions.ru>
 */

namespace WS\Education\Unit1\Task2;
use InvalidArgumentException;

/**
 * Class Server
 * @package WS\Education\Unit1\Task2
 */
class Server {

    private $port;

    /**
     * @var callable[] $callback
     */
    private $handlers = array();
    private $resource;

    /**
     * Server constructor.
     * @param $port
     */
    public function __construct($port)
    {
        $this->resource = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_bind($this->resource, "127.0.0.1", $port);
        socket_listen($this->resource);
    }

    /**
     * @param $handlers
     * @internal param callable $callback
     */
    public function registerHandler($handlers)
    {
        if(!is_callable($handlers)) {
            throw new InvalidArgumentException("callback is not callable");
        }
        $this->handlers[] = $handlers;
    }

    public function listen()
    {
        while ($connect = socket_accept($this->resource)) {
            $socket = new Connection($connect);
            foreach ($this->handlers as $handler) {
                $handler($socket);
            }
        }
    }

    public function close()
    {

    }
}
