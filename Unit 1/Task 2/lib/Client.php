<?php

namespace WS\Education\Unit1\Task2;

/**
 * @author Maxim Sokolovsky <sokolovsky@worksolutions.ru>
 */

class Client {

    private $connect;

    public function __construct($host, $port, $timeout)
    {
        $resource = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
        socket_connect($resource, $host, $port);
        $this->connect = new Connection($resource);
    }
    /**
     * @param string $string
     */
    public function send($string)
    {
        $this->connect->write($string);
    }

    /**
     * @return string
     */
    public function receive()
    {
        return $this->connect->read();
    }

    public function close()
    {
        $this->connect->close();
    }
}