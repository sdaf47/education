<?php
/**
 * @author Maxim Sokolovsky <sokolovsky@worksolutions.ru>
 */

namespace WS\Education\Unit1\Task1;

/**
 * Class Queue
 * @package WS\Education\Unit1\Task1
 */
class Queue implements Collection {

    private $items = array();

    public function __construct($type = null) {}

    /**
     * @param $el
     */
    public function push($el)
    {
        array_push($this->items, $el);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function pop()
    {
        if (empty($this->items)) {
            throw new \Exception("Pop from empty collection");
        }
        return array_shift($this->items);
    }

    /**
     * @return int
     */
    public function size()
    {
        return count($this->items);
    }
}