<?php
/**
 * @author Maxim Sokolovsky <sokolovsky@worksolutions.ru>
 */

namespace WS\Education\Unit1\Task1;

/**
 * Class Stack
 * @package WS\Education\Unit1\Task1
 */
class Stack implements Collection {

    private $items = array();

    public function __construct($type = null) {
        $this->items = array();
    }

    /**
     * @param $el
     * @throws \Exception
     */
    public function push($el)
    {
        array_push($this->items, $el);
    }

    /**
     * @return mixed
     * @throws \Exception
     */
    public function pop()
    {
        if (empty($this->items)) {
            throw new \Exception("Pop from empty collection");
        }
        return array_pop($this->items);
    }

    /**
     * @return int
     */
    public function size()
    {
        return count($this->items);
    }
}